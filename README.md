# Brunch - BackEnd
 Projeto desenvolvido em Java 8

## Baixando o git
 Baixar o git no seguinte link: https://git-scm.com/downloads

## Clonando o Projeto
 Abrir a pasta do git local, clicar com o botão direito e selecionar a opção "git bash here", irá abrir um prompt de comando.
 Com o prompt aberto, execute o seguinte comando: `git clone git@bitbucket.org:FelipeKawassaki/googlecloudapitcc.git`

## Instalando e Configurando o Eclipse
 Primeiro é necessário Baixar a IDE do eclipse no seguinte link: https://www.eclipse.org/downloads/packages/release/oxygen/2/eclipse-ide-eclipse-committers

 Logo em seguida deve com o eclipse aberto, acesse o menu "HELP > ECLIPSE MARCKETPLACE" e pesquisar o seguinte plug-in: "Google Cloud Tool for Eclipse 1.7.1" e instalar;

## Importando o Projeto no eclipse
 No eclipse, clique com o botão direito na aba "Project Explorer"

 Em seguida selecione a opção "Import" logo irá abrir uma janela com as opções de importação

 Selecione a pasta "Maven" e sem seguida a opção "Existing Maven Projects" e selecione o caminho até o arquivo "pom.xml" no projeto clonado anteriormente;

 Finalize o processo de importação ("Next", "Finish")

Com a visão JAVAEEE na aba servers clique na opção para cadastrar um servidor local:

 -- Selecione a Pasta do Google e a Opção "App Engine Standard"

 -- Clique em Next e adcione o war "brunch" no campo "Configured"

 -- Após isso basta finalizar e será criado um serviço local

 -- Então clique com o botão direito nele e selecione a opção start

 
E o servidor será inicializado no link local: `http://localhost:8080/`