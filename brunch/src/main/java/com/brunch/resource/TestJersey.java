package com.brunch.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.brunch.json.instrumenter.JsonHandler;
import com.brunch.model.Estabelecimento;
import com.brunch.model.Usuario;
import com.brunch.service.UsuarioService;

@Provider
@Path("/login")
public class TestJersey implements ContainerResponseFilter{
	
	private UsuarioService usuarioService = new UsuarioService();

	@GET
	@Produces("application/json")
	public List<Estabelecimento> getTestJersey() {
		List<Estabelecimento> estabelecimentos= new ArrayList<Estabelecimento>();
		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setEmail("estabelecimento.email@teste.com.br");
		estabelecimento.setNome("Burger King");
		estabelecimentos.add(estabelecimento);
		return estabelecimentos;
	}
	

	@GET
	@Path("/entrar")
	@Produces(MediaType.APPLICATION_JSON)
	public String login() throws Exception {
		
		String email = "fekawassaki@gmail.com";
		String pass = "$@FPei4dasd3";
        Usuario usuario = usuarioService.buscarUsuariosByEmailEToken(email, pass);
        JsonHandler<Usuario> jsonHandler = new JsonHandler<Usuario>();
        return jsonHandler.handle(usuario);
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add(
                "Access-Control-Allow-Origin", "*");
              responseContext.getHeaders().add(
                "Access-Control-Allow-Credentials", "true");
              responseContext.getHeaders().add(
               "Access-Control-Allow-Headers",
               "origin, content-type, accept, authorization");
              responseContext.getHeaders().add(
                "Access-Control-Allow-Methods", 
                "GET, POST, PUT, DELETE, OPTIONS, HEAD");
	}
}
