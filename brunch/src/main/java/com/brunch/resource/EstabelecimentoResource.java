package com.brunch.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.brunch.json.instrumenter.JsonHandler;
import com.brunch.model.Estabelecimento;
import com.brunch.service.EstabelecimentoService;

@Provider
@Path("/estabelecimento")
public class EstabelecimentoResource {
	
	EstabelecimentoService estabelecimentoService = new EstabelecimentoService();
	
	@POST
	@Path("/salvarEstabelecimento")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public String salvarEstabelecimento(Estabelecimento estabelecimentoRequest) throws Exception {
		Estabelecimento estabelecimento = estabelecimentoService.salvarEstabelecimento(estabelecimentoRequest);
		JsonHandler<Estabelecimento> jsonHandler = new JsonHandler<Estabelecimento>();
		return jsonHandler.handle(estabelecimento);
	}
	
	@GET
	@Path("/buscarEstabelecimentoPorUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public String buscarEstabelecimentoPorUsuario(@QueryParam("idUsuario") Long idUsuario) throws Exception {
		Estabelecimento estabelecimento = estabelecimentoService.buscarEstabelecimentoPorUsuario(idUsuario);
		JsonHandler<Estabelecimento> jsonHandler = new JsonHandler<Estabelecimento>();
		return jsonHandler.handle(estabelecimento);
	}
	
	@GET
	@Path("/buscaEstabelecimentosPorPlacesIds")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public String buscarEstabelecimentoPorPlaceId(@QueryParam("placesId") List<String> placesId) throws Exception {
		List<Estabelecimento> estabelecimento = estabelecimentoService.buscarEstabelecimentoPorPlacesId(placesId);
		JsonHandler<List<Estabelecimento>> jsonHandler = new JsonHandler<List<Estabelecimento>>();
		return jsonHandler.handle(estabelecimento);
	}

}
