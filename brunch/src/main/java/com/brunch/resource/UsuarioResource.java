package com.brunch.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.brunch.dto.Authentication;
import com.brunch.json.instrumenter.JsonHandler;
import com.brunch.model.Usuario;
import com.brunch.service.UsuarioService;

@Provider
@Path("/usuario")
public class UsuarioResource extends CorsFilter {

	UsuarioService usuarioService = new UsuarioService();

	@POST
	@Path("/entrar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public String login(Authentication authenticationRequest) throws Exception {
		Usuario usuario = usuarioService.buscarUsuariosByEmailEToken(authenticationRequest.getEmail(), authenticationRequest.getSenha());
		JsonHandler<Usuario> jsonHandler = new JsonHandler<Usuario>();
		return jsonHandler.handle(usuario);
	}

	@POST
	@Path("/salvar")
	@Produces("application/json")
	@Consumes("application/json")
	public String salvarUsuario(Usuario usuarioRequest) throws Exception {
		Usuario usuario = usuarioService.salvarUsuario(usuarioRequest);
		JsonHandler<Usuario> jsonHandler = new JsonHandler<Usuario>();
		return jsonHandler.handle(usuario);
	}
	
	@POST
	@Path("/atualizar")
	@Produces("application/json")
	@Consumes("application/json")
	public String atualizarUsuario(Usuario usuarioRequest) throws Exception {
		Usuario usuario = usuarioService.atualizarUsuario(usuarioRequest);
		JsonHandler<Usuario> jsonHandler = new JsonHandler<Usuario>();
		return jsonHandler.handle(usuario);
	}
	
	@GET
	@Path("/entrarSocial")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/json")
	public String login(@QueryParam("email") String email) throws Exception {
		Usuario usuarioSocial = usuarioService.buscarUsuarioPorEmail(email);
		JsonHandler<Usuario> jsonHandler = new JsonHandler<Usuario>();
		return jsonHandler.handle(usuarioSocial);
	}

}
