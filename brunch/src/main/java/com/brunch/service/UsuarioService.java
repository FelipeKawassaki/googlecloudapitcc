package com.brunch.service;

import java.util.Date;

import org.mindrot.jbcrypt.BCrypt;

import com.brunch.model.Usuario;
import com.brunch.repository.UsuarioRepository;

public class UsuarioService {

	private UsuarioRepository usuarioRepository = new UsuarioRepository();

	public Usuario buscarUsuariosByEmailEToken(String email, String senha) throws Exception {

		if (email == null && senha == null) {
			throw new Exception("Infome os campos Obrigatórios, email e senha");
		}

		Usuario usuario = usuarioRepository.buscarUsuariosByEmail(email);
		usuario.setPrimeiroAcesso(false);

		if (usuario != null) {
			if (this.checkPass(senha, usuario.getSenha())) {
				return usuario;
			}
		}

		return usuario;
	}

	public Usuario salvarUsuario(Usuario usuario) throws Exception {

		Usuario usuarioDataBase = usuarioRepository.buscarUsuariosByEmail(usuario.getEmail());

		if (usuarioDataBase != null) {
			if (usuarioDataBase.getEmail().equals(usuario.getEmail())) {
				return null;
			}

		}

		if (this.validateUser(usuario)) {

			if (usuario.getId() != null) {
				usuario.setDataCadastro(new Date());
				usuario.getDadosUsuarioSessao().setDataInclusao(new Date());
				usuario.setPrimeiroAcesso(false);
				usuarioRepository.update(usuario);
			} else {
				usuario.setSenha(this.criaToken(usuario.getSenha()));
				usuario.setDataCadastro(new Date());
				usuario.getDadosUsuarioSessao().setDataInclusao(new Date());
				usuario.setPrimeiroAcesso(true);
				usuarioRepository.salvar(usuario);
			}
		}

		return usuario;
	}
	
	public Usuario atualizarUsuario(Usuario usuario) throws Exception {

		Usuario usuarioDataBase = usuarioRepository.buscarUsuariosByEmail(usuario.getEmail());

		if (usuarioDataBase != null) {
			if (usuarioDataBase.getEmail().equals(usuario.getEmail())) {
				return null;
			}
		}

		if (this.validateUser(usuario)) {

			if (usuario.getId() != null) {
				usuario.setDataCadastro(new Date());
				usuario.getDadosUsuarioSessao().setDataInclusao(new Date());
				usuarioRepository.update(usuario);
			}
		}
		return usuario;
	}

	public boolean validateUser(Usuario user) {
		if (user != null) {
			if (user.getEmail() != null && user.getSenha() != null && user.getPrimeiroNome() != null) {
				return true;
			}
		}
		return false;
	}

	public String criaToken(String senha) {
		String token = BCrypt.hashpw(senha, BCrypt.gensalt());
		System.out.println(token);
		return token;
	}

	private boolean checkPass(String plainPassword, String hashedPassword) {
		if (BCrypt.checkpw(plainPassword, hashedPassword)) {
			return true;
		} else {
			return false;
		}
	}

	public Usuario buscarUsuarioPorEmail(String email) throws Exception {
		return usuarioRepository.buscarUsuarioPorEmail(email);
	}

}
