package com.brunch.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.brunch.model.Avaliacao;
import com.brunch.model.Detalhes;
import com.brunch.model.Estabelecimento;
import com.brunch.model.Pagamento;
import com.brunch.model.Usuario;
import com.brunch.repository.AvaliacaoRepository;
import com.brunch.repository.DetalhesRepository;
import com.brunch.repository.EstabelecimentoRepository;
import com.brunch.repository.PagamentoRepository;
import com.brunch.repository.UsuarioRepository;

public class EstabelecimentoService {
	
	UsuarioRepository usuarioRepository = new UsuarioRepository();
	EstabelecimentoRepository estabelecimentoRepository = new EstabelecimentoRepository();
	PagamentoRepository pagamentoRepository = new PagamentoRepository();
	DetalhesRepository detalhesRepository = new DetalhesRepository();
	AvaliacaoRepository avaliacaoRepository = new AvaliacaoRepository();
	
	
	public Estabelecimento salvarEstabelecimento(Estabelecimento estabelecimentoRequest) throws Exception {

		if(this.validateEstabelecimento(estabelecimentoRequest)) {
		
			Estabelecimento estabelecimento = new Estabelecimento();
			
			
			Usuario usuario = usuarioRepository.buscarUsuariosByEmail(estabelecimentoRequest.getUsuario().getEmail());
			
			if(usuario != null) {
				
				if(estabelecimentoRequest.getPagamento() != null) {
					Pagamento pagamento = new Pagamento();
					pagamento.setNomeTitular(estabelecimentoRequest.getPagamento().getNomeTitular());
					pagamento.setCpfTitular(estabelecimentoRequest.getPagamento().getCpfTitular());
					pagamento.setDataVencimento(estabelecimentoRequest.getPagamento().getDataVencimento());
					pagamento.setNumeroCartao(estabelecimentoRequest.getPagamento().getNumeroCartao());
					pagamento.setCodigoVerificador(estabelecimentoRequest.getPagamento().getCodigoVerificador());
					pagamento.setEmail(estabelecimentoRequest.getPagamento().getEmail());
					pagamento.setId(estabelecimentoRequest.getPagamento().getId());
					pagamento.setDataInclusao(new Date());
					
					
					if(pagamento.getId() != null) {
						pagamentoRepository.update(pagamento);
					} else {
						pagamentoRepository.salvar(pagamento);
					}
					
					estabelecimento.setPagamento(pagamento);
				}
				
				estabelecimento.setUsuario(usuario);
				estabelecimento.setEmail(estabelecimentoRequest.getEmail());
				estabelecimento.setNome(estabelecimentoRequest.getNome());
				estabelecimento.setContato(estabelecimentoRequest.getContato());
				estabelecimento.setEndereco(estabelecimentoRequest.getEndereco());
				estabelecimento.setDataInclusao(new Date());
				estabelecimento.setWebsite(estabelecimentoRequest.getWebsite());
				estabelecimento.setPlaceId(estabelecimentoRequest.getPlaceId());
				estabelecimento.setId(estabelecimentoRequest.getId());
			
				if(estabelecimento.getId() != null) {
					estabelecimentoRepository.update(estabelecimento);
				} else {
					estabelecimentoRepository.salvar(estabelecimento);
				}

				Estabelecimento estabelecimentoSalvo = estabelecimentoRepository.buscarEstabelecimentosPlaceId(estabelecimento.getPlaceId());
				
				if(!estabelecimentoRequest.getDetalhes().isEmpty()) {
					for(Detalhes detalhes : estabelecimentoRequest.getDetalhes()) {
						
						detalhes.setEstabelecimento(estabelecimentoSalvo);
						detalhes.setDataInclusao(new Date());

						if(detalhes.getId() != null) {
							detalhesRepository.update(detalhes);
						} else {
							detalhesRepository.salvar(detalhes);
						}						
						estabelecimentoSalvo.getDetalhes().add(detalhes);
					}
				}
				
				if(estabelecimentoRequest.getAvaliacao() != null) {
					if(!estabelecimentoRequest.getAvaliacao().isEmpty()) {
						for(Avaliacao avaliacao : estabelecimentoRequest.getAvaliacao()) {
							avaliacao.setUsuario(usuario);
							avaliacao.setDataInclusao(new Date());
							avaliacao.setEstabelecimento(estabelecimentoSalvo);
							avaliacaoRepository.salvar(avaliacao);
							estabelecimentoSalvo.getAvaliacao().add(avaliacao);
						}
					}
				}

				
				return estabelecimentoSalvo;
	
			}
		}
		return null;
	}

	
	public boolean validateEstabelecimento(Estabelecimento estabelecimento) {
		
		if(StringUtils.isNotEmpty(estabelecimento.getPlaceId()) && StringUtils.isNotEmpty(estabelecimento.getNome()) && StringUtils.isNotEmpty(estabelecimento.getEndereco())
			&& StringUtils.isNotEmpty(estabelecimento.getContato()) && StringUtils.isNotEmpty(estabelecimento.getEmail()) && !(estabelecimento.getDetalhes().isEmpty())) {
			return true;
		}
		return false;
	}


	public Estabelecimento buscarEstabelecimentoPorUsuario(Long idUsuario) throws Exception {
		return estabelecimentoRepository.buscarEstabelecimentoPorUsuario(idUsuario);
	}
	
	public Estabelecimento buscarEstabelecimentoPorPlaceId(String placeId) throws Exception {
		return estabelecimentoRepository.buscarEstabelecimentoPorPlaceId(placeId);
	}
	
	public List<Estabelecimento> buscarEstabelecimentoPorPlacesId(List<String> placesId) throws Exception {
		return estabelecimentoRepository.buscarEstabelecimentoPorPlaceId(placesId);
	}


}

