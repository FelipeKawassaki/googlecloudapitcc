package com.brunch.enumerate;

public enum Perfil {

	PROPRIETARIO(1, "Proprietario"),
	CLIENTE(0, "Cliente");

	private final int id;
	private final String tipo;

	Perfil(int id, String tipo) {
		this.id = id;
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

}
