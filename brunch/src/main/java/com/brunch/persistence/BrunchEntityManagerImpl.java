/**
 * PCO - Planejamento e Controle da Operação
 *
 * Copyright 2017 RumoAll, Inc. and/or its affiliates, and individual contributors as indicated by
 * the @author tags.
 *
 * In case of private module the following applies: Private license under elaboration, see
 * rumoall.com for more information.
 *
 *
 * In case of Open Source Module the following applies: Licensed under the APACHE License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.brunch.persistence;

import java.util.List;

import javax.persistence.EntityManager;

public class BrunchEntityManagerImpl<T> implements BrunchEntityManager<T> {
    public void persist(EntityManager em, T entity) throws Exception {
        synchronized (em) {
            em.persist(entity);
        }
    }

    public void persist(final EntityManager em, final List<T> entities) throws Exception {
        synchronized (em) {
            for (T entity : entities) {
                em.persist(entity);
            }
        }
    }

    public void update(EntityManager em, T entity) throws Exception {
        synchronized (em) {
            em.merge(entity);
        }
    }

    public void delete(EntityManager em, T entity) throws Exception {
        synchronized (em) {
            em.remove(entity);
        }
    }

    @Override
    public void delete(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass) {
        synchronized (em) {
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    final T toDelete = (T) em.find(typeClass, entityTuple.getId());
                    em.remove(toDelete);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
            }
        }
    }

    @Override
    public void update(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass)
            throws Exception {
        synchronized (em) {
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    em.merge(entityTuple.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
            }
        }
    }

    @Override
    public void update(EntityManager em, List<EntityTuple<T>> entities) throws Exception {
        synchronized (em) {
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    em.merge(entityTuple.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
            }
        }
    }
}
