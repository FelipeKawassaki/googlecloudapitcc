package com.brunch.persistence;

public class EntityTuple<T> {
    private T instance;
    private long id;

    public EntityTuple(T instance, long id) {
        super();
        this.instance = instance;
        this.id = id;
    }

    public Class<T> getValue() {
        final Class<T> type = (Class<T>) instance.getClass();
        return type;
    }

    public EntityTuple<T> setInstance(final T instance) {
        this.instance = instance;
        return this;
    }

    public long getId() {
        return id;
    }

    public EntityTuple<T> setId(final long id) {
        this.id = id;
        return this;
    }
}