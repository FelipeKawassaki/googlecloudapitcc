package com.brunch.persistence;

import java.util.List;

import javax.persistence.EntityManager;

public interface BrunchEntityManager<T> {

    void persist(EntityManager em, T entity) throws Exception;

    void persist(EntityManager em, List<T> entities) throws Exception;

    void delete(EntityManager em, T entity) throws Exception;

    void delete(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass);

    void update(EntityManager em, T entity) throws Exception;

    void update(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass)
            throws Exception;

    void update(EntityManager em, List<EntityTuple<T>> entities) throws Exception;
	
}
