/**
 * PCO - Planejamento e Controle da Operação
 *
 * Copyright 2017 RumoAll, Inc. and/or its affiliates, and individual contributors as indicated by
 * the @author tags.
 *
 * In case of private module the following applies: Private license under elaboration, see
 * rumoall.com for more information.
 *
 *
 * In case of Open Source Module the following applies: Licensed under the APACHE License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.brunch.persistence;

import java.util.List;

import javax.persistence.EntityManager;

public class BrunchEntityManagerImplTA<T> implements BrunchEntityManager<T> {
    public void persist(final EntityManager em, final T entity) throws Exception {
        synchronized (em) {
            boolean rolledBack = false;
            try {
                em.getTransaction().begin();
                em.persist(entity);
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }

    public void persist(final EntityManager em, final List<T> entities) throws Exception {
        synchronized (em) {
            em.getTransaction().begin();
            boolean rolledBack = false;
            try {
                for (T entity : entities) {
                    em.persist(entity);
                }
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }

    public void update(final EntityManager em, final T entity) throws Exception {
        synchronized (em) {
            boolean rolledBack = false;
            try {
                em.getTransaction().begin();
                em.merge(entity);
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }


    public void delete(EntityManager em, T entity) throws Exception {
        synchronized (em) {
            boolean rolledBack = false;
            try {
                em.getTransaction().begin();
                em.remove(entity);
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public void delete(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass) {
        synchronized (em) {
            boolean rolledBack = false;
            em.getTransaction().begin();
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    final T toDelete = (T) em.find(typeClass, entityTuple.getId());
                    em.remove(toDelete);
                }
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public void update(EntityManager em, List<EntityTuple<T>> entities, Class<T> typeClass)
            throws Exception {
        synchronized (em) {
            boolean rolledBack = false;
            em.getTransaction().begin();
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    em.merge(entityTuple.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public void update(EntityManager em, List<EntityTuple<T>> entities) throws Exception {
        synchronized (em) {
            boolean rolledBack = false;
            em.getTransaction().begin();
            try {
                for (EntityTuple<T> entityTuple : entities) {
                    em.merge(entityTuple.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
                em.getTransaction().rollback();
                rolledBack = true;
                throw e;
            } finally {
                if (!rolledBack) {
                    em.getTransaction().commit();
                }
            }
        }
    }
}
