package com.brunch.json.instrumenter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.UnsupportedEncodingException;
import java.io.IOException;

public class JsonHandler<T> {
    public JsonHandler() {
        super();
    }

    public String handle(final T data) {
        return serialize(data);
    }

    public String serialize(final T data) {
        final ObjectMapper mapper = new ObjectMapper();
        String jsonString = "NOT_INITIALIZED";
        try {
            jsonString = mapper.writeValueAsString(data);
        } catch (Exception e) {
            jsonString = "ERROR";
            e.printStackTrace();
        }
        return jsonString;
    }

    public String serialize(final T data, final String encoding) throws UnsupportedEncodingException {
        final ObjectMapper mapper = new ObjectMapper();
        String jsonString = "NOT_INITIALIZED";
        try {
            jsonString = mapper.writeValueAsString(data);
        } catch (Exception e) {
            jsonString = "ERROR";
            e.printStackTrace();
        }
        return new String(jsonString.getBytes(), encoding);
    }

    public T derialize(final String data, TypeReference<T> typeReference) {
        T result = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            result = mapper.readValue(data.getBytes(), typeReference);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return result;
    }
}
