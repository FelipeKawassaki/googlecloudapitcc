package com.brunch.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tb_usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "ID_USUARIO", nullable = false, unique = true)
	private Long id;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "ID_DADOS_USUARIO_SESSAO")
	private DadosUsuarioSessao dadosUsuarioSessao;
	
	@JsonIgnore
	@OneToMany(targetEntity = Estabelecimento.class, mappedBy="usuario")
	private List<Estabelecimento> estabelecimento = new ArrayList<>();
	
	@Column(name = "EMAIL", nullable = false, unique = true)
	private String email;
	
	@Column(name = "SENHA")
	private String senha;
	
	@Column(name = "PRIMEIRO_NOME")
	private String primeiroNome;
	
	@Column(name = "SEGUNDO_NOME")
	private String segundoNome;
	
	@Column(name = "DATA_INCLUSAO")
	private Date dataCadastro;
	
	transient private boolean isPrimeiroAcesso;
	

	public Usuario() {

	}
	
	public boolean isPrimeiroAcesso() {
		return isPrimeiroAcesso;
	}

	public void setPrimeiroAcesso(boolean isPrimeiroAcesso) {
		this.isPrimeiroAcesso = isPrimeiroAcesso;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public DadosUsuarioSessao getDadosUsuarioSessao() {
		return dadosUsuarioSessao;
	}


	public void setDadosUsuarioSessao(DadosUsuarioSessao dadosUsuarioSessao) {
		this.dadosUsuarioSessao = dadosUsuarioSessao;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public String getPrimeiroNome() {
		return primeiroNome;
	}


	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}


	public String getSegundoNome() {
		return segundoNome;
	}


	public void setSegundoNome(String segundoNome) {
		this.segundoNome = segundoNome;
	}


	public Date getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public List<Estabelecimento> getEstabelecimento() {
		return estabelecimento;
	}


	public void setEstabelecimento(List<Estabelecimento> estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

}

