package com.brunch.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_estabelecimento")

public class Estabelecimento {
	
	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "ID_ESTABELECIMENTO", nullable = false, unique = true)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_USUARIO")
	private Usuario usuario;
	
	@OneToOne
	@JoinColumn(name="ID_PAGAMENTO")
	private Pagamento pagamento;
	
	@OneToMany(targetEntity = Detalhes.class, mappedBy="estabelecimento", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Detalhes> detalhes = new ArrayList<Detalhes>();
	
	@OneToMany(targetEntity = Avaliacao.class, mappedBy="estabelecimento", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Avaliacao> avaliacao = new ArrayList<Avaliacao>();
	
	@OneToMany(targetEntity = Busca.class, mappedBy="estabelecimento", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<Busca> busca = new ArrayList<Busca>();
	
	@Column(name = "PLACE_ID")
	private String placeId;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "CONTATO")
	private String contato;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "WEBSITE")
	private String website;
	
	@Column(name="ENDERECO")
	private String endereco;
	
	@Column(name ="DATA_INCLUSAO")
	private Date dataInclusao;
	

	public Estabelecimento() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public List<Detalhes> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<Detalhes> detalhes) {
		this.detalhes = detalhes;
	}

	public List<Avaliacao> getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(List<Avaliacao> avaliacao) {
		this.avaliacao = avaliacao;
	}

	public List<Busca> getBusca() {
		return busca;
	}

	public void setBusca(List<Busca> busca) {
		this.busca = busca;
	}
}
