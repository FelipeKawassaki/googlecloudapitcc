package com.brunch.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.brunch.model.Pagamento;
import com.brunch.persistence.BrunchEntityManager;
import com.brunch.persistence.BrunchEntityManagerFactory;

public class PagamentoRepository {
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("connection");
	private EntityManager em = emf.createEntityManager();
	
	public Pagamento salvar(final Pagamento pagamento) throws Exception {
        try {
        	this.em.joinTransaction();
            BrunchEntityManager<Pagamento> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.persist(this.em, pagamento);
            return pagamento;
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Salvar o Usuário: "  + e);
        }
    }
	
	public Pagamento update(final Pagamento pagamento) throws Exception {
        try {
        	this.em.joinTransaction();
            BrunchEntityManager<Pagamento> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.update(this.em, pagamento);
            return pagamento;
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Salvar o Pagamento: "  + e);
        }
    }


}
