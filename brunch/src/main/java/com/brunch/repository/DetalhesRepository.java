package com.brunch.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.brunch.model.Detalhes;
import com.brunch.persistence.BrunchEntityManager;
import com.brunch.persistence.BrunchEntityManagerFactory;

public class DetalhesRepository {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("connection");
	private EntityManager em = emf.createEntityManager();
	
	public Detalhes salvar(final Detalhes detalhes) throws Exception {
        try {
        	this.em.joinTransaction();
            BrunchEntityManager<Detalhes> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.persist(this.em, detalhes);
            return detalhes;
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Salvar o Detalhes: "  + e);
        }
    }
	
	public Detalhes update(final Detalhes detalhes) throws Exception {
        try {
        	this.em.joinTransaction();
            BrunchEntityManager<Detalhes> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.update(this.em, detalhes);
            return detalhes;
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Salvar o Detalhes: "  + e);
        }
    }

	@SuppressWarnings("unchecked")
	public List<Detalhes> buscarDetalhesPorEstabelecimento(Long idEstabelecimento) throws Exception {
		try {

			this.em.joinTransaction();

			String jpql = "select d from Detalhes d								" +
						"	where d.idEstabelecimento = :idEstabelecimento		" ;

			Query q = this.em.createQuery(jpql);
			q.setParameter("idEstabelecimento", idEstabelecimento);

			List<Detalhes> resultList = q.getResultList() != null ? q.getResultList() : new ArrayList<>();

			return resultList;

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Detalhes: " + e);
		}
	}

	public void delete(Detalhes detalhes) throws Exception {
		try {
        	this.em.joinTransaction();
            BrunchEntityManager<Detalhes> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.delete(this.em, detalhes);
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Deletar o Detalhes: "  + e);
        }
		
	}

}
