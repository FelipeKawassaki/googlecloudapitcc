package com.brunch.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.brunch.model.Avaliacao;
import com.brunch.persistence.BrunchEntityManager;
import com.brunch.persistence.BrunchEntityManagerFactory;

public class AvaliacaoRepository {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("connection");
	private EntityManager em = emf.createEntityManager();
	
	public Avaliacao salvar(final Avaliacao avaliacao) throws Exception {
        try {
        	this.em.joinTransaction();
            BrunchEntityManager<Avaliacao> ctEm = BrunchEntityManagerFactory.getnIstance();
            ctEm.persist(this.em, avaliacao);
            return avaliacao;
            
        } catch (Exception e) {
        	throw new Exception("Erro ao Salvar o Avaliacao: "  + e);
        }
    }

}
