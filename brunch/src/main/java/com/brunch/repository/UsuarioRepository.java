package com.brunch.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.brunch.model.Usuario;
import com.brunch.persistence.BrunchEntityManager;
import com.brunch.persistence.BrunchEntityManagerFactory;

public class UsuarioRepository {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("connection");
	private EntityManager em = emf.createEntityManager();

	@SuppressWarnings("unchecked")
	public Usuario buscarUsuariosByEmail(String email) throws Exception {

		try {

			this.em.joinTransaction();

			String jpql = "select u from Usuario u" + "	where u.email = :email";

			Query q = this.em.createQuery(jpql);
			q.setParameter("email", email);

			List<Usuario> resultList = q.getResultList();

			if (!resultList.isEmpty()) {
				for (int i = 0; i <= resultList.size();) {
					return resultList.get(0);
				}
			}

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Usuário: " + e);
		}
		return null;
	}

	public Usuario salvar(final Usuario usuario) throws Exception {
		try {
			this.em.joinTransaction();
			BrunchEntityManager<Usuario> ctEm = BrunchEntityManagerFactory.getnIstance();
			ctEm.persist(this.em, usuario);
			return usuario;

		} catch (Exception e) {
			throw new Exception("Erro ao Salvar o Usuário: " + e);
		}
	}

	public Usuario update(final Usuario usuario) throws Exception {
		try {
			this.em.joinTransaction();
			BrunchEntityManager<Usuario> ctEm = BrunchEntityManagerFactory.getnIstance();
			ctEm.update(this.em, usuario);
			return usuario;

		} catch (Exception e) {
			throw new Exception("Erro ao Salvar o Usuário: " + e);
		}
	}

	@SuppressWarnings("unchecked")
	public Usuario buscarUsuarioPorEmail(String email) throws Exception {
		try {

			this.em.joinTransaction();

			String jpql = "select u from Usuario u" + "	where u.email = :email";

			Query q = this.em.createQuery(jpql);
			q.setParameter("email", email);

			List<Usuario> usuarios = q.getResultList();
			if(!usuarios.isEmpty()) {
				return usuarios.get(0);
			} else {
				return null;
			}

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Usuário: " + e);
		}
	}

}
