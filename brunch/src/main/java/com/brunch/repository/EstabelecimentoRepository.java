package com.brunch.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.brunch.model.Estabelecimento;
import com.brunch.model.Pagamento;
import com.brunch.persistence.BrunchEntityManager;
import com.brunch.persistence.BrunchEntityManagerFactory;

public class EstabelecimentoRepository {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("connection");
	private EntityManager em = emf.createEntityManager();

	public Estabelecimento salvar(final Estabelecimento estabelecimento) throws Exception {
		try {
			this.em.joinTransaction();
			BrunchEntityManager<Estabelecimento> ctEm = BrunchEntityManagerFactory.getnIstance();
			ctEm.persist(this.em, estabelecimento);
			return estabelecimento;

		} catch (Exception e) {
			throw new Exception("Erro ao Salvar o Estabelecimento: " + e);
		}
	}

	public Estabelecimento update(final Estabelecimento estabelecimento) throws Exception {
		try {
			this.em.joinTransaction();
			BrunchEntityManager<Estabelecimento> ctEm = BrunchEntityManagerFactory.getnIstance();
			ctEm.update(this.em, estabelecimento);
			return estabelecimento;

		} catch (Exception e) {
			throw new Exception("Erro ao Salvar o Estabelecimento: " + e);
		}
	}

	@SuppressWarnings("unchecked")
	public Estabelecimento buscarEstabelecimentosPlaceId(String placeId) throws Exception {
		try {

			this.em.joinTransaction();

			String jpql = "select es from Estabelecimento es" + "	where es.placeId = :placeId";

			Query q = this.em.createQuery(jpql);
			q.setParameter("placeId", placeId);

			List<Estabelecimento> resultList = q.getResultList();

			if (!resultList.isEmpty()) {
				return resultList.get(0);
			}

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Estabelecimento: " + e);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Pagamento buscarPagamentosPorEstabelecimento(Long idEstabelecimento) throws Exception {
		try {

			this.em.joinTransaction();

			String jpql = "select p from Estabelecimento e		" +
						"	join fetch p.pagamento p			" + 
						"	where e.id = :idEstabelecimento		" ;

			Query q = this.em.createQuery(jpql);
			q.setParameter("idEstabelecimento", idEstabelecimento);

			List<Pagamento> resultList = q.getResultList();

			if (!resultList.isEmpty()) {
				return resultList.get(0);
			}

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Estabelecimento: " + e);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Estabelecimento buscarEstabelecimentoPorUsuario(Long idUsuario) throws Exception {
		try {

			this.em.joinTransaction();

			String jpql = "select es from Estabelecimento es" + "	where es.usuario.id = :idUsuario";

			Query q = this.em.createQuery(jpql);
			q.setParameter("idUsuario", idUsuario);

			List<Estabelecimento> resultList = q.getResultList();

			if (!resultList.isEmpty()) {
				return resultList.get(0);
			}

		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Estabelecimento: " + e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Estabelecimento buscarEstabelecimentoPorPlaceId(String placeId) throws Exception {
		try {
		
			this.em.joinTransaction();
			
			String jpql = "select es from Estabelecimento es" + "	where es.placeId = :placeId";
			
			Query q = this.em.createQuery(jpql);
			q.setParameter("placeId", placeId);
			
			List<Estabelecimento> resultList = q.getResultList();
		
			if (!resultList.isEmpty()) {
				return resultList.get(0);
			}
			
		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Estabelecimento: " + e);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Estabelecimento> buscarEstabelecimentoPorPlaceId(List<String> placesId) throws Exception {
		try {
		
			this.em.joinTransaction();
			
			String jpql = "select es from Estabelecimento es" 
			+ "	where es.placeId in(:placesId)";
			

			
			Query q = this.em.createQuery(jpql);
			q.setParameter("placesId", placesId);
			
			List<Estabelecimento> resultList = q.getResultList();
		
			return resultList;
			
		} catch (Exception e) {
			throw new Exception("Erro ao Buscar o Estabelecimento: " + e);
		}
	}

}
